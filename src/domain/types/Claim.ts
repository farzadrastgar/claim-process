import IncidentTypes from "./IncidentTypes"
import Policy from "./Policy"

type Claim = {
  claimId: string
  policy: Policy
  claimant: {
    name: string
    email: string
    phone: string
  }
  incidentDate: string
  incidentType: IncidentTypes
  claimedAmount: number
}

export default Claim
