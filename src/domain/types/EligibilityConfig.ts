import IncidentTypes from "./IncidentTypes"

type EligibilityConfig = {
  [key in IncidentTypes]: {
    maxClaimAmount: number
  }
}

export default EligibilityConfig
