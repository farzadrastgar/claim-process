import { CheckEligibilityUseCase } from "./application/useCases/CheckEligibilityUseCase"
import { EligibilityService } from "./domain/services/EligibilityService"
import eligibilityConfig from "../config/eligibilityConfig.json"
import { FileClaimRepository } from "./infrastructure/repositories/FileClaimRepository"
import { CliPresentation } from "./presentation/cli"
import { ClaimValidatorService } from "./domain/services/ClaimValidatorService"

// Get CLI path from command line arguments
const cliPath = process.argv[2]

/**
 * Main function to start the application.
 * @param filePath The path to the file containing claims data.
 * @returns A Promise that resolves once the application execution is completed.
 */
export async function main(filePath: string): Promise<void> {
  // Initialize claim repository with file path
  const claimRepository = new FileClaimRepository(filePath)

  // Initialize eligibility service with eligibility configuration
  const eligibilityService = new EligibilityService(eligibilityConfig)

  // Initialize claim validator service
  const claimValidatorService = new ClaimValidatorService()

  // Initialize CheckEligibilityUseCase with dependencies
  const checkEligibilityUseCase = new CheckEligibilityUseCase(
    claimRepository,
    claimValidatorService,
    eligibilityService
  )

  // Initialize CLI presentation with CheckEligibilityUseCase
  const cliPresentation = new CliPresentation(checkEligibilityUseCase)

  // Output summary using CLI presentation
  cliPresentation.outputSummary()
}

// Call main function with CLI path and handle any errors
main(cliPath).catch((err) => console.error(err))
