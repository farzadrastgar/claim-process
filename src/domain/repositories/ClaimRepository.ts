import Claim from "../types/Claim"

// Interface representing a claim repository
export interface ClaimRepository {
  /**
   * Retrieves all claims from the repository.
   * @returns A Promise that resolves to an array of claims.
   */
  getAll(): Promise<Claim[]>
}
