type Policy = {
  policyNumber: string
  coveragePeriod: {
    startDate: string
    endDate: string
  }
}

export default Policy
