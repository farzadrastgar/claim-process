import Policy from "../types/Policy"
import EligibilityConfig from "../types/EligibilityConfig"
import IncidentTypes from "../types/IncidentTypes"

// Define the shape of eligibility result
export interface EligibilityResult {
  eligible: boolean
  reason?: string
}

/**
 * Service class to check eligibility of claims
 */
export class EligibilityService {
  // Maximum claim amount configured
  private maxClaimAmount: number

  /**
   * Constructor to create an instance of EligibilityService.
   * @param eligibilityConfig The eligibility configuration for different incident types.
   */
  constructor(readonly eligibilityConfig: EligibilityConfig) {}

  /**
   * Check eligibility for a claim based on various factors.
   * @param claimedAmount The claimed amount for the claim.
   * @param incidentDate The incident date for the claim.
   * @param incidentType The type of incident for the claim.
   * @param policy The policy details associated with the claim.
   * @returns The result of eligibility check.
   */
  public checkEligibility(
    claimedAmount: number,
    incidentDate: string,
    incidentType: IncidentTypes,
    policy: Policy
  ): EligibilityResult {
    // Check if the incident type is supported
    if (!(incidentType in this.eligibilityConfig)) {
      return {
        eligible: false,
        reason: `Invalid incidentType: ${incidentType}`,
      }
    }

    // Retrieve max claim amount for the incident type
    this.maxClaimAmount = this.eligibilityConfig[incidentType].maxClaimAmount

    // Check if claimed amount exceeds the maximum allowed
    if (claimedAmount > this.maxClaimAmount) {
      return { eligible: false, reason: "Claim amount exceeds the threshold" }
    }

    // Check if incident date falls within policy coverage period
    if (
      incidentDate < policy.coveragePeriod.startDate ||
      incidentDate > policy.coveragePeriod.endDate
    ) {
      return {
        eligible: false,
        reason: "Incident date is outside the policy coverage period",
      }
    }

    // If all checks pass, claim is eligible
    return { eligible: true }
  }
}
