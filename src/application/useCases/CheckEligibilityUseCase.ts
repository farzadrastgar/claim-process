import { ClaimRepository } from "../../domain/repositories/ClaimRepository"
import { EligibilityService } from "../../domain/services/EligibilityService"
import { ClaimValidatorService } from "../../domain/services/ClaimValidatorService"

// Define the shape of claim summary
export interface ClaimSummary {
  claimId: string
  eligible: boolean
  reason?: string
}

/**
 * Class to perform eligibility checks on claims
 */
export class CheckEligibilityUseCase {
  /**
   * Constructor to instantiate the CheckEligibilityUseCase.
   * @param claimRepository The claim repository for retrieving claims.
   * @param claimValidatorService The service for validating claims.
   * @param eligibilityService The service for checking claim eligibility.
   */

  constructor(
    private readonly claimRepository: ClaimRepository,
    private readonly claimValidatorService: ClaimValidatorService,
    private readonly eligibilityService: EligibilityService
  ) {}

  /**
   * Execute the use case.
   * @returns A Promise that resolves to an array of claim summaries.
   */
  async execute(): Promise<ClaimSummary[]> {
    // Get all claims from the claim repository
    const claims = await this.claimRepository.getAll()

    // Initialize an array to store the summary of claims
    const claimsSummary: ClaimSummary[] = []

    // Iterate through each claim
    for (const claim of claims) {
      // Destructure claim properties
      const { claimId, claimedAmount, incidentDate, incidentType, policy } =
        claim

      // Initialize eligibility with default value
      let eligibility: Omit<ClaimSummary, "claimId"> = { eligible: false }

      try {
        // Validate the claim
        let claimValidationError =
          this.claimValidatorService.validateClaim(claim)

        // If claim is valid, check eligibility
        if (!claimValidationError) {
          eligibility = this.eligibilityService.checkEligibility(
            claimedAmount,
            incidentDate,
            incidentType,
            policy
          )
        } else {
          // If claim is invalid, mark as ineligible with reason
          eligibility = {
            eligible: false,
            reason: claimValidationError,
          }
        }
      } catch (err) {
        // Catch any unexpected errors
        eligibility = {
          eligible: false,
          reason: "Unexpected Error",
        }

        // Log the claim and error for debugging
        console.log(claim)
        console.error(err)
      } finally {
        // Add claim summary to the list
        claimsSummary.push({
          claimId,
          ...eligibility,
        })
      }
    }

    // Return the summary of claims
    return claimsSummary
  }
}
