import { CheckEligibilityUseCase } from "../../application/useCases/CheckEligibilityUseCase"

/*
 * Presentation class for CLI output
 */
export class CliPresentation {
  /**
   * Constructor to create an instance of CliPresentation.
   * @param checkEligibilityUseCase The use case for checking eligibility.
   */
  constructor(private checkEligibilityUseCase: CheckEligibilityUseCase) {}

  /**
   * Output summary of claims eligibility.
   * @returns A Promise that resolves once the summary is output.
   */
  async outputSummary(): Promise<void> {
    // Execute the check eligibility use case
    const claimsSummary = await this.checkEligibilityUseCase.execute()

    // Output the summary to the console
    console.log(claimsSummary)
  }
}
