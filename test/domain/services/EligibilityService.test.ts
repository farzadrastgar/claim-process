import {
  EligibilityResult,
  EligibilityService,
} from "../../../src/domain/services/EligibilityService"
import EligibilityConfig from "../../../src/domain/types/EligibilityConfig"
import IncidentTypes from "../../../src/domain/types/IncidentTypes"
import Policy from "../../../src/domain/types/Policy"

const mockEligibilityConfig: EligibilityConfig = {
  ACCIDENT: {
    maxClaimAmount: 1000,
  },
  THEFT: {
    maxClaimAmount: 2000,
  },
  HEALTH: {
    maxClaimAmount: 500,
  },
}

describe("EligibilityService", () => {
  let eligibilityService: EligibilityService
  const policy: Policy = {
    policyNumber: "POL001",
    coveragePeriod: {
      startDate: "2023-01-01",
      endDate: "2023-12-31",
    },
  }

  beforeEach(() => {
    eligibilityService = new EligibilityService(mockEligibilityConfig)
  })

  it("should return eligible if claimed amount is within the threshold", () => {
    const result: EligibilityResult = eligibilityService.checkEligibility(
      500,
      "2023-06-15",
      IncidentTypes.ACCIDENT,
      policy
    )
    expect(result.eligible).toBe(true)
  })

  it("should return not eligible with reason when claimed amount exceeds the threshold", () => {
    const result: EligibilityResult = eligibilityService.checkEligibility(
      15000,
      "2023-06-15",
      IncidentTypes.THEFT,
      policy
    )
    expect(result.eligible).toBe(false)
    expect(result.reason).toBe("Claim amount exceeds the threshold")
  })

  it("should return not eligible with reason when incident date is outside policy coverage period", () => {
    const result: EligibilityResult = eligibilityService.checkEligibility(
      500,
      "2024-06-15",
      IncidentTypes.HEALTH,
      policy
    )
    expect(result.eligible).toBe(false)
    expect(result.reason).toBe(
      "Incident date is outside the policy coverage period"
    )
  })
})
