## Installation

```bash

$ npm i

```

## Running the app

```bash

$ npm run start ../../../data/claims.json

```

## Test

```bash
# unit tests
$ npm run test

```

## Assumptions

- Maximum claim amount whould be different based on incidentType
- All currencies are in USD
- Eligibility rules can be configured via ./config/eligibilityConfig.json
- Claims data like dates and amount or type could be unvalid.
