enum IncidentTypes {
  ACCIDENT = "ACCIDENT",
  THEFT = "THEFT",
  HEALTH = "HEALTH",
}

export default IncidentTypes
