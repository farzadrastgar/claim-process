import * as fs from "fs"
import path from "path"
import Claim from "../../domain/types/Claim"
import { ClaimRepository } from "../../domain/repositories/ClaimRepository"

/**
 * Repository class to interact with claims stored in a file
 */
export class FileClaimRepository implements ClaimRepository {
  private filePath

  /**
   * Constructor to create an instance of FileClaimRepository.
   * @param filePath The path to the file containing claims data.
   */
  constructor(filePath: string) {
    this.filePath = filePath
  }

  /**
   * Retrieve all claims from the file.
   * @returns A Promise that resolves to an array of claims.
   */
  async getAll(): Promise<Claim[]> {
    // Read file synchronously and parse claims data
    const rawData = fs.readFileSync(
      path.resolve(__dirname, this.filePath),
      "utf-8"
    )
    const claims: Claim[] = JSON.parse(rawData)
    return claims
  }
}
