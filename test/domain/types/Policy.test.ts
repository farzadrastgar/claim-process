import Policy from "../../../src/domain/types/Policy"

describe("Policy", () => {
  test("Policy has correct structure", () => {
    const policy: Policy = {
      policyNumber: "P123456",
      coveragePeriod: {
        startDate: "2024-01-01",
        endDate: "2024-12-31",
      },
    }

    expect(policy.policyNumber).toEqual("P123456")
    expect(policy.coveragePeriod.startDate).toEqual("2024-01-01")
    expect(policy.coveragePeriod.endDate).toEqual("2024-12-31")
  })
})
