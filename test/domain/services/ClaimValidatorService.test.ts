import { ClaimValidatorService } from "../../../src/domain/services/ClaimValidatorService"
import Claim from "../../../src/domain/types/Claim"
import IncidentTypes from "../../../src/domain/types/IncidentTypes"

describe("ClaimValidatorService", () => {
  let claimValidatorService: ClaimValidatorService

  beforeEach(() => {
    claimValidatorService = new ClaimValidatorService()
  })

  afterEach(() => {
    jest.clearAllMocks() // Clear mock calls after each test
  })

  const validClaim: Claim = {
    claimId: "CLM001",
    policy: {
      policyNumber: "POL001",
      coveragePeriod: {
        startDate: "2024-01-01",
        endDate: "2024-12-31",
      },
    },
    claimant: {
      name: "Jane Smith",
      email: "jane@example.com",
      phone: "987-654-3210",
    },
    incidentType: IncidentTypes.ACCIDENT,
    incidentDate: "2024-03-19",
    claimedAmount: 1000,
  }

  describe("validateClaim method", () => {
    it("should return null for valid claim", () => {
      const isValidDateFormatMock = jest.spyOn(
        claimValidatorService as any,
        "isValidDateFormat"
      )
      isValidDateFormatMock.mockReturnValue(true)
      expect(claimValidatorService.validateClaim(validClaim)).toBeNull()
    })

    it("should return error for invalid coverage period date", () => {
      const isValidDateFormatMock = jest.spyOn(
        claimValidatorService as any,
        "isValidDateFormat"
      )
      isValidDateFormatMock
        .mockReturnValueOnce(true)
        .mockReturnValueOnce(false)
        .mockReturnValueOnce(true)

      const policy = {
        policyNumber: "POL001",
        coveragePeriod: {
          startDate: "2024-01-01",
          endDate: "31-12-2024",
        },
      }

      const invalidCoveragePeriodClaim: Claim = { ...validClaim, policy }
      expect(
        claimValidatorService.validateClaim(invalidCoveragePeriodClaim)
      ).toEqual("Invalid coverage period date")
    })

    it("should return error for invalid incident date", () => {
      const isValidDateFormatMock = jest.spyOn(
        claimValidatorService as any,
        "isValidDateFormat"
      )
      isValidDateFormatMock
        .mockReturnValueOnce(true)
        .mockReturnValueOnce(true)
        .mockReturnValueOnce(false)
      const invalidIncidentDateClaim: Claim = {
        ...validClaim,
        incidentDate: "invalidDate",
      }
      expect(
        claimValidatorService.validateClaim(invalidIncidentDateClaim)
      ).toEqual("Invalid incident date")
    })

    it("should return error for invalid claimed amount", () => {
      const invalidClaimedAmountClaim: Claim = {
        ...validClaim,
        claimedAmount: -100,
      }

      expect(
        claimValidatorService.validateClaim(invalidClaimedAmountClaim)
      ).toEqual("Invalid claimed amount")
    })
  })

  describe("isValidDateFormat", () => {
    it("should return true for valid date format", () => {
      const validDate = "2024-03-19"
      expect(claimValidatorService["isValidDateFormat"](validDate)).toBe(true)
    })

    describe("invalid date format detection", () => {
      it("should return false for invalid date format 'DD-MM-YYYY'", () => {
        const invalidDate = "19-03-2024" // Invalid format
        expect(claimValidatorService["isValidDateFormat"](invalidDate)).toBe(
          false
        )
      })

      it("should return false for invalid date format 'YYYY MM DD'", () => {
        const invalidDate = "2024 03 19" // Invalid format
        expect(claimValidatorService["isValidDateFormat"](invalidDate)).toBe(
          false
        )
      })

      it("should return false for invalid date format 'DD Mon YYYY'", () => {
        const invalidDate = "19 Mar 2024" // Invalid format
        expect(claimValidatorService["isValidDateFormat"](invalidDate)).toBe(
          false
        )
      })

      it("should return false for invalid date format 'YYYY/MM/DD'", () => {
        const invalidDate = "2024/03/12" // Invalid format
        expect(claimValidatorService["isValidDateFormat"](invalidDate)).toBe(
          false
        )
      })

      it("should return false for invalid date format 'Unix Timestamp'", () => {
        const invalidDate = "1647715200" // Invalid format
        expect(claimValidatorService["isValidDateFormat"](invalidDate)).toBe(
          false
        )
      })
    })

    it("should return false for empty string", () => {
      const emptyString = ""
      expect(claimValidatorService["isValidDateFormat"](emptyString)).toBe(
        false
      )
    })

    it("should return false for null", () => {
      const nullString = "null"
      expect(claimValidatorService["isValidDateFormat"](nullString)).toBe(false)
    })

    it("should return false for undefined", () => {
      const undefinedString = "undefined"
      expect(claimValidatorService["isValidDateFormat"](undefinedString)).toBe(
        false
      )
    })
  })
})
