import EligibilityConfig from "../../../src/domain/types/EligibilityConfig"
import IncidentTypes from "../../../src/domain/types/IncidentTypes"

describe("EligibilityConfig", () => {
  test("should have valid configuration for all incident types", () => {
    // Actual eligibility configuration
    const config: EligibilityConfig = {
      ACCIDENT: { maxClaimAmount: 10000 },
      THEFT: { maxClaimAmount: 10000 },
      HEALTH: { maxClaimAmount: 10000 },
    }

    // Assert that the configuration is valid for all incident types
    Object.keys(config).forEach((incidentType) => {
      expect(incidentType in IncidentTypes).toBe(true) // Ensure incident type is valid
      expect(
        config[incidentType as keyof EligibilityConfig].maxClaimAmount
      ).toBeGreaterThan(0) // Ensure maxClaimAmount is valid
    })
  })
})
