import Claim from "../../../src/domain/types/Claim"
import IncidentTypes from "../../../src/domain/types/IncidentTypes"
import Policy from "../../../src/domain/types/Policy"

describe("Claim", () => {
  it("should have correct structure", () => {
    const policy: Policy = {
      policyNumber: "P123456",
      coveragePeriod: {
        startDate: "2024-01-01",
        endDate: "2024-12-31",
      },
    }

    const incidentType = IncidentTypes.ACCIDENT

    const claim: Claim = {
      claimId: "C123",
      policy: policy,
      claimant: {
        name: "John Doe",
        email: "john@example.com",
        phone: "1234567890",
      },
      incidentDate: "2024-03-19",
      incidentType: incidentType,
      claimedAmount: 1000,
    }

    expect(claim.claimId).toEqual("C123")
    expect(claim.policy).toEqual(policy)
    expect(claim.claimant.name).toEqual("John Doe")
    expect(claim.claimant.email).toEqual("john@example.com")
    expect(claim.claimant.phone).toEqual("1234567890")
    expect(claim.incidentDate).toEqual("2024-03-19")
    expect(claim.incidentType).toEqual(incidentType)
    expect(claim.claimedAmount).toEqual(1000)
  })
})
