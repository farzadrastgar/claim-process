import Claim from "../types/Claim"

/**
 * Service class to validate claims.
 */
export class ClaimValidatorService {
  /**
   * Validates a claim.
   * @param claim The claim to validate.
   * @returns A validation error message if any, otherwise null.
   */
  public validateClaim(claim: Claim): string | null {
    // Check coverage period date validity
    if (
      !this.isValidDateFormat(claim.policy.coveragePeriod.startDate) ||
      !this.isValidDateFormat(claim.policy.coveragePeriod.endDate)
    ) {
      return "Invalid coverage period date"
    }

    // Check incident date validity
    if (
      !claim.incidentDate ||
      typeof claim.incidentDate !== "string" ||
      !this.isValidDateFormat(claim.incidentDate)
    ) {
      return "Invalid incident date"
    }

    // Check claimed amount validity
    if (isNaN(claim.claimedAmount) || claim.claimedAmount <= 0) {
      return "Invalid claimed amount"
    }

    // No validation errors
    return null
  }

  /**
   * Checks if the given date string is in valid date format (YYYY-MM-DD).
   * @param dateString The date string to validate.
   * @returns True if the date string is in valid format, otherwise false.
   */
  private isValidDateFormat(dateString: string): boolean {
    const regex = /^\d{4}-\d{2}-\d{2}$/
    const date = new Date(dateString)

    return regex.test(dateString) && !isNaN(date.getTime())
  }
}
