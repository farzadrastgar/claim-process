import {
  CheckEligibilityUseCase,
  ClaimSummary,
} from "../../../src/application/useCases/CheckEligibilityUseCase"
import { ClaimValidatorService } from "../../../src/domain/services/ClaimValidatorService"
import { EligibilityService } from "../../../src/domain/services/EligibilityService"
import IncidentTypes from "../../../src/domain/types/IncidentTypes"

const eligibilityConfig = {
  ACCIDENT: {
    maxClaimAmount: 10000,
  },
  THEFT: {
    maxClaimAmount: 10000,
  },
  HEALTH: {
    maxClaimAmount: 10000,
  },
}

const claim = {
  claimId: "CLM001",
  policy: {
    policyNumber: "POL001",
    coveragePeriod: {
      startDate: "2024-01-01",
      endDate: "2024-12-31",
    },
  },
  claimant: {
    name: "Jane Smith",
    email: "jane@example.com",
    phone: "987-654-3210",
  },
  incidentType: IncidentTypes.ACCIDENT,
  incidentDate: "2024-03-19",
  claimedAmount: 1000,
}

class MockClaimRepository {
  async getAll() {
    return [claim]
  }
}

class MockClaimValidatorService extends ClaimValidatorService {
  validateClaim = jest.fn()
}

class MockEligibilityService extends EligibilityService {
  checkEligibility = jest.fn()
}

describe("CheckEligibilityUseCase", () => {
  let useCase: CheckEligibilityUseCase
  let claimValidatorService: MockClaimValidatorService
  let eligibilityService: MockEligibilityService

  beforeEach(() => {
    const claimRepository = new MockClaimRepository()
    claimValidatorService = new MockClaimValidatorService()
    eligibilityService = new MockEligibilityService(eligibilityConfig)
    useCase = new CheckEligibilityUseCase(
      claimRepository,
      claimValidatorService,
      eligibilityService
    )
  })

  it("should return claim summaries for an eligible claim", async () => {
    claimValidatorService.validateClaim.mockReturnValueOnce(null)
    eligibilityService.checkEligibility.mockReturnValueOnce({ eligible: true })
    const claimSummaries: ClaimSummary[] = await useCase.execute()
    expect(claimSummaries).toHaveLength(1)
    expect(claimSummaries[0].claimId).toEqual(claim.claimId)
    expect(claimSummaries[0].eligible).toEqual(true)
  })

  it("should return claim summaries for an ineligible claim", async () => {
    claimValidatorService.validateClaim.mockReturnValueOnce(null)
    eligibilityService.checkEligibility.mockReturnValueOnce({ eligible: false })
    const claimSummaries: ClaimSummary[] = await useCase.execute()
    expect(claimSummaries).toHaveLength(1)
    expect(claimSummaries[0].claimId).toEqual(claim.claimId)
    expect(claimSummaries[0].eligible).toEqual(false)
  })

  it("should return claim summaries for a claim with failed validation", async () => {
    claimValidatorService.validateClaim.mockReturnValueOnce("Error")
    eligibilityService.checkEligibility.mockReturnValueOnce({ eligible: true })
    const claimSummaries: ClaimSummary[] = await useCase.execute()
    expect(claimSummaries).toHaveLength(1)
    expect(claimSummaries[0].claimId).toEqual(claim.claimId)
    expect(claimSummaries[0].eligible).toEqual(false)
    expect(claimSummaries[0].reason).toEqual("Error")
  })

  it("should return claim summaries for an unexpected error in checkEligibility service", async () => {
    claimValidatorService.validateClaim.mockReturnValueOnce(null)
    eligibilityService.checkEligibility.mockImplementationOnce(() => {
      throw new Error("Eligibility check failed")
    })
    const claimSummaries: ClaimSummary[] = await useCase.execute()

    expect(claimSummaries).toHaveLength(1)
    expect(claimSummaries[0].claimId).toEqual(claim.claimId)
    expect(claimSummaries[0].eligible).toEqual(false)
    expect(claimSummaries[0].reason).toEqual("Unexpected Error")
  })

  it("should return claim summaries for an unexpected error in validateClaim service", async () => {
    claimValidatorService.validateClaim.mockImplementationOnce(() => {
      throw new Error("Validation check failed")
    })
    eligibilityService.checkEligibility.mockReturnValueOnce({ eligible: true })
    const claimSummaries: ClaimSummary[] = await useCase.execute()

    expect(claimSummaries).toHaveLength(1)
    expect(claimSummaries[0].claimId).toEqual(claim.claimId)
    expect(claimSummaries[0].eligible).toEqual(false)
    expect(claimSummaries[0].reason).toEqual("Unexpected Error")
  })
})
